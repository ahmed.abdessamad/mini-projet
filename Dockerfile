FROM openjdk:8

ENV Port=8090

EXPOSE $Port

ADD target/reservation-service-0.0.1-SNAPSHOT.jar  reservation-service-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","reservation-service-0.0.1-SNAPSHOT.jar"]
